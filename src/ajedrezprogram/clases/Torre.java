/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ajedrezprogram.clases;

/**
 *
 * @author Adri
 */
public class Torre extends Pieza{

    public Torre(Posicion posicion, boolean blanca) {
        super(posicion, blanca);
    }

    @Override
    public String saludar() {
        String color = super.isBlanca()==true ? "blanco" : "negro";
        return "Soy una torre " + color + "situada en la posicion" + super.getPosicion();
    }

    @Override
    public boolean mover(Posicion p) {
        if(super.mover(p)==true){
            if(super.getPosicion().getX()==p.getX() || super.getPosicion().getY()==p.getY()){
                return true;
            }else{
                return false;
            }
        }else{
            return false;
        }
    }
    
    
    
    
}
