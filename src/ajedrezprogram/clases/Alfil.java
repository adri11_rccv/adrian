/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ajedrezprogram.clases;

import static java.lang.Math.abs;

/**
 *
 * @author Adri
 */
public class Alfil extends Pieza{

    public Alfil(Posicion posicion, boolean blanca) {
        super(posicion, blanca);
    }
    
    
    @Override
    public String saludar(){
        String color = super.isBlanca()==true ? "blanco" : "negro";
        return "Soy un alfil" + color + "situado en la posicion" + super.getPosicion();
    }

    @Override
    public boolean mover(Posicion p) {
        if(super.mover(p)==true){
            if((abs(super.getPosicion().getX()-p.getX())==abs(super.getPosicion().getY()-p.getY()))){
                return true;
            }else{
                return false;
            }
        }else{
            return false;
        }
    }
    
    
    
    
    
   
}
