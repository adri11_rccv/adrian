/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ajedrezprogram.clases;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Adri
 */
public abstract class Pieza {
    private final Posicion posicion;
    private final boolean blanca;
    
    private final List<Pieza> piezas= new ArrayList<>();

    protected Pieza(Posicion posicion, boolean blanca) {
        this.posicion = posicion;
        this.blanca = blanca;
    }

    public Posicion getPosicion() {
        return posicion;
    }

    public boolean isBlanca() {
        return blanca;
    }
    
    public boolean mover(Posicion p){
        if(p==null){
            return false;
        }else if(this.posicion.equals(p)){
            return false;
        }
        return true;
    }
    
    public int movimientos(boolean mover, Posicion posicion){
        int fichasMovidas =0;
        for(Pieza p : piezas){
            if(p.mover(posicion)==mover){
                fichasMovidas++;
            }
        }
        
        return fichasMovidas;
    }
    
    
    public abstract String saludar();
        
    
    
}
